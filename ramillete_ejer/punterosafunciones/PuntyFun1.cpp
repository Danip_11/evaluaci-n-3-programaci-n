#include <stdio.h>
#include <stdlib.h>

double area_triangulo(double n1, double n2){                                    //Crea las funciones con las formulas de sus áreas
    return (n1*n2)/2;
}
double area_rectangulo(double n1, double n2){
    return n1*n2;
}
double (*eleccion[])(double n1, double n2)={&area_rectangulo,&area_triangulo};  //Crea un puntero que va a contener las direcciones de la funciones que calculan el área
                                                                                //Y que le van a pasar dos datos tipo double

int main(){
    double b,a;
    int elige;
    
    printf("1.�rea Triangulo\n2.�rea Rectangulo\n");
    scanf("%i",&elige);
    
    printf("b: ");
    scanf("%lf",&b);
    printf("a: ");
    scanf("%lf",&a);

    

    printf("%.2lf",(*eleccion[elige-1])(b,a));                                     //Depende de la opcion elegida el puntero llamara a una funci�n u otra y le pasara los datos necesario para ejecutarla
                                                                                   //elige-1 es porque va a empezar siempre por el 0 y al pasar el numero de la opci�n elegida va a ejecutar la siguiente y no la que corresponde  

    return EXIT_SUCCESS; 
}
