#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double polinomio(double n1,double n2){
    return n1*n2;
}
double exponencial(double n1,double n2){
    return pow(n2,n1);
}
double (*eleccion[])(double n1, double n2)={&polinomio,&exponencial};   //Crea un puntero que va a contener las direcciones de la funciones
                                                                        //Y que le van a pasar dos datos tipo double
int main(){
    double x,a;
    int opcion;
    
    printf("1.Polinomio\n2.Funci�n exponencial\n\n");
    scanf("%i",&opcion);
    
    printf("x: ");
    scanf("%lf",&x);
    printf("a: ");
    scanf("%lf",&a);
    

    printf("%.2lf",(*eleccion[opcion-1])(x,a));                         //Depende de la opcion elegida el puntero llamara a una funci�n u otra y le pasara los datos necesario para ejecutarla
                                                                        //opcion-1 es porque va a empezar siempre por el 0 y al pasar el numero de la opci�n elegida va a ejecutar la siguiente y no la que corresponde
 

    return EXIT_SUCCESS;
}

