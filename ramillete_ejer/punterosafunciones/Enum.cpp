#include <stdio.h>
#include <stdlib.h>

int suma(int n1,int n2){
	return n1 + n2;
}
int resta (int n1,int n2){
	return n1 - n2;
}
int (*puntero[])(int n1,int n2) = {&suma,&resta};

enum TFuncion{
	sumar,				//Le pone un nombre al 0, el siguiente al 1 ...etc 
	restar
};

int main(){
	int n1 = 3;
	int n2 = 2;
	
	printf("%i",(*puntero[restar])(n1,n2));

	return EXIT_SUCCESS;
}
