#include <stdio.h>
#include <stdlib.h>

void imprimir (char *letra) {
    if (*letra != '\0') {			//Condici�n de salida, si donde apunta letra es diferente de /0 continua
		imprimir (letra+1);			//Llama otra vez a la funci�n y le pasa letra incrementado en 1 		
        printf ("%c", *letra);		//Imprime letra       			
    }
}

int main () {
    char cadena[] = "dabale arroz a la zorra el abad";

    imprimir (cadena);

    return EXIT_SUCCESS;
}

