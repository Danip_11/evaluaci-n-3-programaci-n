#include <stdio.h>
#include <stdlib.h>

#define MIN .0001

int factorial (int n) {
    if ( n == 0 )
        return 1;

    return n * factorial (n-1);
}

double operacion_e (unsigned n) {  
    double  siguiente = 1.00 / factorial (n);
    	
    if ( siguiente < MIN )				//Condici�n de salida, cuando el numero es mas peque�o que el minimo establecido sale
        return siguiente;   
    else
		return siguiente + operacion_e (n+1);
}

int main () {
	int n = 0;
	
	printf("N�mero para empezar a calcular: ");
    scanf("%i",&n);
	
    printf ("e = %.4lf\n", operacion_e (n));

    return EXIT_SUCCESS;
}

