#include <stdio.h>
#include <stdlib.h>

int factorial (int n){
    if (n <= 0)						//Comprueba si el n�mero es menor o igual a 0
        return 1;					//Si es as� devuelve un 1
    return n * factorial (n-1);		//Si no es menor que 0 multiplica el n�mero por la salida de la funci�n a la cual se le pasa el numero menos 1

}

int main () {

    int n;
    
    printf("Introduce un n�mero para realizar el factorial: ");
    scanf("%i",&n);

    printf ("El factorial de (%i) es %i\n", n, factorial (n));  //Imprime los resultados

    return EXIT_SUCCESS;
}


