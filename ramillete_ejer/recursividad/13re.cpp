#include <stdio.h>
#include <stdlib.h>

const char *ordinal_name[3][10] = {
    {"", "primero", "segundo", "tercero", "cuarto", "quinto", "sexto",
     "septimo", "octavo", "noveno"},
    {"", "decimo", "vigesimo", "trigesimo", "cuatrigesimo",
     "quincuagesimo", "sexagesimo", "septuagesimo", "octogesimo",
     "nonagesimo"},
    {"", "centesimo", "bicentesimo", "tricentesimo", "cuadrigentesimo",
     "quingentesimo", "sexgentesino", "septingentesimo",
     "octingentesimo", "noningentesimo"}
};

void ordinal (int n, int fila) {
    if ( n != 0 ) {										//Si el numero es diferente de 0 continua
        ordinal (n / 10, fila+1);						//Divide el numero entre 10 y a la fila le suma 1
        												//Ej:5    ordinal(5/10=0,0+1=1) --> llama de nuevo a la funci�n pero como n ya vale cero pasa a imprimir
        printf ("%s ", ordinal_name[fila][n % 10]);		//Imprime de la fila 1(del primero al noveno), la quinta celda (0,1,2,3,4,5) y en la celda 5 esta escrito "quinto"
    }
}

int main(){
    int n = 0;
    
    printf("Numero a ordinal: ");
    scanf("%i",&n);

    ordinal (n,0);

    printf ("\n");

    return EXIT_SUCCESS;
}

