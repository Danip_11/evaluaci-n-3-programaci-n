#include <stdio.h>
#include <stdlib.h>

int factorial_num (int num) {
    if ( num == 0 )
        return 1;

    return num * factorial_num (num-1);
}

double operacion_e (int num) { 
    if ( num == 0 )			
        return 1;
    return 1.00 / factorial_num (num) + operacion_e (num-1);
}

int main () {
	 int n;
    
    printf("N�mero para empezar a calcular: ");
    scanf("%i",&n);
    
    printf ("e = %.4lf\n", operacion_e (n));

    return EXIT_SUCCESS;
}


