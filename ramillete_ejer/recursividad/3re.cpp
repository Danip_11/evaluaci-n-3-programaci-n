#include <stdio.h>
#include <stdlib.h>

void imprimir (int n) {

    if (n<0)				//Si el n�mero es menor que cero termina
        return;

    printf ("%i ", n);		//Imprime el numero actual 
    imprimir (n-1);			//Llama a la funci�n y le pasa el n�mero actual menos 1
}

int main () {
	
    int n;
    
    printf("Introduce un n�mero: ");
    scanf("%i",&n);

    imprimir (n);			//Llama a la funci�n imprimir y le pasa el n�mero introducido
    printf ("\n");

    return EXIT_SUCCESS;
}

