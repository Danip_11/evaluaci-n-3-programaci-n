#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TOPE 10

int factorial (int n) {
    if ( n == 0 )
        return 1;

    return n * factorial (n-1);
}

void s (int n, double res[TOPE]) {
   res[n] = pow (-1, n) / factorial (n);        //Va guardando los resultados en el array res

   if ( n > 0 )                                
    s (n-1, res);
}

int main () {
    double res[TOPE];

    s (TOPE, res);

    for (int i=0; i<TOPE; i++)                  //El for recorre el array y va imprimiendo los resultados almacenados
        printf ("%.4lf  ", res[i]);

    return EXIT_SUCCESS;
}

