#include <stdio.h>
#include <stdlib.h>

int suma (int n){
    return n + (n <= 0 ? 0: suma (n-1) );   	//Devuelve el resultado del calculo, se usa un operador condicional ternario

}

int main () {

    int n;
    
    printf("Introduce un n�mero para realizar la suma: ");
    scanf("%i",&n);

    printf ("suma (%i) = %i\n", n, suma (n));	//LLama a la funci�n e imprime el resultado 

    return EXIT_SUCCESS;
}


