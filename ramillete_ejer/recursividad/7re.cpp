#include <stdio.h>
#include <stdlib.h>

void comprobar_divisores (int numero,int divisor ){  //Saca los divisores de un numero sin contarse el propio n�mero 
	
	//if(numero<=1)
	//printf ("%i ", numero); 
	
    if (divisor < 1)  								//Condici�n de salida
        return;  

    if (numero % divisor == 0)						//Si el resto del numero entre el divisor es 0, imprime el divisor
        printf ("%i ", divisor);

    comprobar_divisores (numero, --divisor);		//Vuelve a llamar a la funci�n restando uno al divisor hasta que se cumpla la condici�n de salida
}

int main () {

    int n;
    
    printf("Introduce un n�mero: ");
    scanf("%i",&n);

    printf ("Los divisores de %i son: \n", n);
    comprobar_divisores (n, n/2);

    return EXIT_SUCCESS;
}


