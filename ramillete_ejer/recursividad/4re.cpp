#include <stdio.h>
#include <stdlib.h>

void imprimir (int n) {

    if (n<0)				//Condici�n de salida
        return;

    imprimir (n-1);			//Para que empiece por el 0, va llamando a la funci�n hasta que n es 0
    printf ("%i ", n);
}

int main () {

    int n;

	printf("Introduce un n�mero: ");
    scanf("%i",&n);
    
    imprimir (n);

    printf ("\n");

    return EXIT_SUCCESS;
}



