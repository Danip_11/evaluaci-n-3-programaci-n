#include <stdio.h>
#include <stdlib.h>

#define ERROR 0.0001

bool mismo_signo (double a, double b) { return a * b >= 0; }		//Multiplica los valores y verifica los signos dependiendo del resultado obtenido

double f (double a[], int grado, double x) {						
    double resultado = 0;
    double potencia = 1;

    for (int i = 0; i<=grado; i++, potencia*=x)
        resultado += potencia * a[i];

    return resultado;
}

void preguntar (double coe[], int grado, double *li, double *ls) {
    double fli = 0, fls = 0;
    do {
    	system("cls");
        printf ("RAIZ\n\n");
        printf("Introduce dos valores con distinto signo.\n\n");

        printf ("f(%.2lf) = %.2lf\n", *li, fli);					
        printf ("f(%.2lf) = %.2lf\n", *ls, fls);
		printf ("\n");	
        printf ("Limite Inferior: ");
        scanf ("%lf", li);
        fli = f(coe, grado, *li);									

        printf ("Limite Superior: ");
        scanf ("%lf", ls);
        fls = f(coe, grado, *ls);
    } while (mismo_signo ( fli, fls ) );							//Va a seguir preguntando si los resultados tienen el mismo signo

}

double buscar_cero (double coe[], int grado, double li, double ls) {
    double medio = (ls + li) / 2;

    if (ls - li < ERROR)
       return medio;

    if ( mismo_signo (f(coe, grado, li), f(coe, grado, medio) ) )
        li = medio;
    else
        ls = medio;

    return buscar_cero (coe, grado, li, ls);
}

int main () {

    int grado = 2;
    double coe[] = {-48, 8, 1}; 
    double li, ls;           

    preguntar (coe, grado, &li, &ls);
    printf ("Raiz: %.4lf\n", li, ls,
            buscar_cero (coe, grado, li, ls) );

    return EXIT_SUCCESS;
}

