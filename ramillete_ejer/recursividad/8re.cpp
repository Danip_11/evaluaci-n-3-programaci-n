#include <stdio.h>
#include <stdlib.h>

bool numero_de_divisores  (int numero,int divisor) {
    if (divisor < 2)			//Condicion de salida
    return false;

    if(numero % divisor == 0)	//Si el resto entre el numero y el divisor es 0 devuelve el numero
        return numero;
    else 
        numero_de_divisores (numero, divisor - 1);	//Si no vuelve a llamarse a si misma pasandose el numero y el divisor -1
}

bool es_primo (int n) {
    if(numero_de_divisores (n, n/2)==false)			//Si numero de divisores devuelve false quiere decir que es primo, entonces esta funcion devuelve true, en caso de no ser primo false
    	return true;
    else
    	return false; 
}

int main () {
    int n;
    
    printf("Introduce un n�mero: ");
    scanf("%i",&n);

	if(es_primo(n)==true)							//Si la funcion de es_primo devuelve verdadero imprime que es primo, si no lo es imprime que ese numero no es primo
    printf ("%i si es primo.\n",n);
    else
    printf ("%i no es primo.\n",n);					

    return EXIT_SUCCESS;
}




