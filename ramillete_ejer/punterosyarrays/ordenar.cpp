#include <stdio.h>
#include <stdlib.h>

#define N 10

void mostrar (int *p[N]) { //funci�n que recibe un array de punteros a enteros
    for (int i=0; i<N; i++)
        printf ("%i ", *p[i]);
    printf ("\n");
}

int main () {
    int numeros[N] = {2,5,3,7,4,1,6,8,9,0}; // array con los n�meros a ordenar
    int *p[N]; 						  //array de punteros

    for (int i = 0; i < N; i++)
        p[i] = &numeros[i];				//guarda la direcci�n de cada celda del array en las celdas del array de punteros correspondiente

    mostrar (p);					//pasa el array de punteros a mostrar
    
    for (int x = 0; x < N - 1; x++)		
        for (int i = 0; i < N - 1; i++)
            if (*p[i] < *p[i + 1]) {	//si el n�mero que hay almacenado donde apunta *p[i] es mayor que el que hay almacenado en la siguiente celda 
                int *aux = p[i + 1];	//*aux apuntar� a �l  
                p[i + 1] = p[i];		//guarda el m�s peque�o en la siguiente celda
                p[i] = aux;				// y el m�s grande en la anterior 
            }
	mostrar (p);


    return EXIT_SUCCESS;
}
/*
N = 4

2,4,1,5		x = 0
			i = 0
4,2,1,5		x = 0
			i = 1
4,2,1,5		x = 0
			i = 2

4,2,5,1		x = 1
			i = 0
4,2,5,1		x = 1
			i = 1
4,5,2,1		x = 1
			i = 2

5,4,2,1		x = 2
			i = 0
5,4,2,1		x = 2
			i = 1
5,4,2,1		x = 2
			i = 2
*/



			

