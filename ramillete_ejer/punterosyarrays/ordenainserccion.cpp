#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

int main () {
	int numeros[N];
	srand(time(NULL));
	
	for(int i = 0; i < N; i++)
		numeros[i] = rand() % 10 + 1;
	
	for(int i = 0; i < N; i++)
		printf("%2i ", numeros[i]);
	
	for(int i = 1; i < N; i++)
		for(int j = 0; j < i; j++)
			if(numeros[i] < numeros[j]) {
				int aux = numeros[i];
				numeros[i] = numeros[j];
				numeros[j] = aux;
			}
			
	printf("\n");

	for(int i = 0; i < N; i++)
		printf("%2i ", numeros[i]);
	
    return EXIT_SUCCESS;
}

