#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

int main () {
	int numeros[N];
	int *p[N];
	srand(time(NULL));
	
	for(int i = 0; i < N; i++)
		numeros[i] = rand() % 10 + 1;
	
	printf("Array original\n");
	for(int i = 0; i < N; i++)
		printf("%2i ", numeros[i]);
		
	for(int i = 0; i < N; i++)
		p[i] = &numeros[i];
	
	printf("\n\n");
	
	printf("Array punteros desordenado\n");
	for(int i = 0; i < N; i++)
		printf("%2i ", *p[i]);
		
	for(int i = 1; i < N; i++)
		for(int j = 0; j < i; j++)
			if(*p[i] < *p[j]) {
				int *aux = p[i];
				p[i] = p[j];
				p[j] = aux;
			}
			
	printf("\n\n");

	printf("Array punteros ordenado\n");
	for(int i = 0; i < N; i++)
		printf("%2i ", *p[i]);
	
    return EXIT_SUCCESS;
}
