#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10
#define NV 3

void titulo(){
    system("clear");
    system ("toilet -fpagga --gay GAME");
    printf("\n\n"); 
}

void series(){
    int serie1[N];
    int serie2[N];
    int serie3[N];
    int *array_ptr[N] = {serie1,serie2,serie3};
    int res;
    int nv = 0;
    int tope = N/2;
    
    serie1[0] = 2;
    serie2[0] = 3;
    serie3[0] = 100;
    
    for (int i = 1; i < N; i++)
        serie1[i] = serie1[i - 1] * 2;
    
    for (int i = 1; i < N; i++)
        serie2[i] = serie2[i - 1] + 3;
    
    for (int i = 1; i < N; i++)
        serie3[i] = serie3[i - 1] - 5;

    for(; nv < NV; nv++) {
        printf("\n\n");
        printf("FASE %i \n", nv+1);
        tope = N/2;
        int i = 0;
        do{
            for(i = 0; i < tope; i++)
                printf("%i -->", *(array_ptr[nv]+i));
            scanf("%i",&res);
            if(res == *(array_ptr[nv] + i))
                printf("Correcto\n");
            else {
                printf("Intentalo de nuevo\n");
                if(tope < N - 1)
                    tope++;
            }
        }while(res != *(array_ptr[nv] + i));
    }
    
    if(nv == NV){
        printf("Juego Finalizado\n");	
	}
}   

void ganar() {
    printf("Has pasado a la siguiente ronda\n");
    series();  
}

void perder() {
    printf("No pasas a la siguiente ronda. Intentalo de nuevo\n");
    exit(1);
}                 

int main(){
    srand(time(NULL));                              //Semilla, esto permite que la función "rand" genere números diferentes dependiendo del momento en el que se ejecute
    int random = rand() % 2;                        //Genera numeros aleatorios, en este caso 0 o 1
    int num = 0;
	void (*ptr_fun[])() = {&ganar,&perder};                      
    
    do{
        //Interfaz
        
        titulo();
        
        for(int fila=0; fila<8; fila++) {                    
            for(int i = 0; i < 2; i++) {
                for(int columna=0; columna<8; columna++)
                    printf("*");
                printf("\t");                           //Esto permite que cuando termine de imprimir una fila de * tabule y repita lo mismo, 
            }                                           //de forma que cree dos rectángulos iguales uno al lado del otro
            printf("\n");
            
        }
        printf("\n");
        printf("Puerta 1\tPuerta 2");
        
        printf("\n\n");
        printf("Elige una puerta (1 o 2): ");
        scanf("%i", &num);
        printf("\n\n");
        
    }while(num > 2 || num <= 0);
    
    (*ptr_fun[random])();                                    //Utiliza el puntero a funciones pasandole un número aletorio (0 o 1) y dependiendo del numero "p" llamará una función u otra
    
    return EXIT_SUCCESS;
}
