#include<stdio.h>
#include<stdlib.h>

#define N 3

int sale1 (int num) {
	return num % N;
}

int sale2 (int num) {
	if (num < 0)
		return num + N;
	return num;
}


int main(){
  double matriz[N][N] = {
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9}
	};
	double res1 = 0, temp = 1, res2 = 0;

	printf("La matriz es:\n");
	printf("{1, 2, 3}\n{4, 5, 6}\n{7, 8, 9}\n\n");
	
	for (int f = 0; f < N; f++) {
		temp = 1;
		for (int c = 0; c < N; c++) 
			temp *= matriz[sale1(c + f)][sale1(c)];
		res1 += temp;
	}
	res2 = 0;
	for (int f = N - 1; f >= 0; f--) {
		temp = 1;
		for (int c = N - 1; c >= 0; c--) {
			temp *= matriz[sale2(c - f)][sale2(c)];
		}
		res2 += temp;
	}
	printf ("Resultado1: %9.2lf.\nResultado2: %9.2lf.\n\n", res1, res2);
	printf ("Determinante: %9.2lf.\n", res1 - res2);

return EXIT_SUCCESS;
}




