#include <stdio.h>
#include <stdlib.h>
#include <cstring>

int main(){
	char texto[]={'a','b','c','d','e'};
	int i = 0;
	
	printf("Los caracteres {a,b,c,d,e} en ASCII son: \n");

	//for (int i = 0; i < strlen(texto); i++)
	while(texto[i] !='\0' || i < strlen(texto)){
		printf("%c --> %3d \n",texto[i],texto[i]);
		i++;
	}
	return EXIT_SUCCESS;
}
