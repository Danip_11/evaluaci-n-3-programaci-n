#include <stdio.h>
#include <stdlib.h>

int main(){
	int temp=0;
	
	printf("Escribe una temperatura: ");
	scanf(" %i",&temp);
	
	if(temp<=0){
		printf("El estado del agua a temperatura %i es Hielo.\n",temp);
	}else if(temp>=100){
		printf("El estado del agua a temperatura %i es Vapor.\n",temp);
	}else{
		printf("El estado del agua a temperatura %i es Liquido.\n",temp);
	}
	
	//Con un switch no era capaz de realizarlo ya que al no admitir rangos solo
	//se me ocurria insertar todos loas valores de uno en uno pero no me parecia 
	//una opcion valida. 
	
	return EXIT_SUCCESS;
}