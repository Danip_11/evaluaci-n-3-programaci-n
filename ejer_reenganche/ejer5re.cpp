#include <stdio.h>
#include <stdlib.h>
#include <cstring>

int main(){
	char texto[]={'a','b','c','d','e'};
	
printf("Los caracteres {a,b,c,d,e} en ASCII son: \n");

	for (int i = 0; i < strlen(texto); i++)
	{
		printf("%c --> %3d \n",texto[i],texto[i]);
	}
	return EXIT_SUCCESS;
}