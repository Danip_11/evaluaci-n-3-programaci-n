#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(){
	char color;
	
	do{	
		printf("Escribe una de estas letras R,G,B: \n");
		scanf(" %c",&color);

		
		switch(color){
			case 'R':
			printf("Has escrito la letra %c en mayusculas.",tolower(color));
			break;
			case 'G':
			printf("Has escrito la letra %c en mayusculas.",tolower(color));
			break;
			case 'B':
			printf("Has escrito la letra %c en mayusculas.",tolower(color));
			break;
			default:
			printf("Has escrito una letra no valida.\n\n");		
		}
	}while(color!='R' && color!='G' && color!='B' );

	return EXIT_SUCCESS;
}