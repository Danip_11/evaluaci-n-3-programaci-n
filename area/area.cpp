#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#define MAX_ERROR .001


double f (double *polinomio, double grado, double x) {

    double altura = 0,
           potencia = 1;

    for (int p=0; p<grado; p++, potencia*=x)
        altura += polinomio[p] * potencia;

    return altura;
}

double *pedir (int *dim) {
    double buffer, *pol = NULL;
    char end[2]; /* Un par�ntesis y un \0*/

    *dim = 0;
    printf ("Introduce el Polinomio ej: (2.5 1 4): ");
    scanf (" %*[(]");

    scanf (" %*[(]");

    do {
        pol = (double *) realloc(pol, (*dim+1) * sizeof(double));
        scanf(" %lf", &buffer);
        pol[(*dim)++] = buffer;
    } while (!scanf(" %1[)]", end));

    return pol;
}

void mostrar (double *p, int d) {
    for (int i=d-1; i>0; i--)
        printf ("%.2lfx^(%i) + ",p[i], i);
    printf ("%.2lf\n", p[0]);
}

double integral (double *p, int d, int li, int ls, double inc) {
    double suma = 0;
    for (double x=li; x<ls; x+=inc)
        suma += f(p, d, x);
    suma *= inc;

    return suma;
}

int main (int argc, char *argv[]) {
    double *pol, li, ls;
    int dim;               
    char opt;              
    int otro_pol;          
    int otra_area;         
    double area, ultima_area;

        pol = pedir (&dim);
                            
            printf ("\n");
            printf ("Limite inferior: ");
            scanf ("%lf", &li);
            printf ("Limite superior: ");
            scanf ("%lf", &ls);

            if (li > ls) { 
                double aux = li;  
                li = ls;
                ls = aux;
            }

            // C�lculos 
            double inc = fabs (ls - li);  
            
            area = integral (pol, dim, li, ls, inc);   
            ultima_area = area - 5 * MAX_ERROR;       

            for (inc/=2; fabs (area - ultima_area) > MAX_ERROR; inc/=2) {
                ultima_area = area;
                area = integral (pol, dim, li, ls, inc);
            }
            printf ("\n");
            printf ("El �rea de:\n");
            mostrar (pol, dim);
            printf ("entre [%.2lf, %.2lf] es: \n", li, ls);
            printf ("\nArea = %.3lf\n", area);
            printf ("\n");
            printf ("\n");

    return EXIT_SUCCESS;
}

