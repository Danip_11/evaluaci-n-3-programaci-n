#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x20

struct TPersonaje {
    char letra;
    int municion;
    double vida;
    char *nombre;
};

void muestra (struct TPersonaje p) {
    printf (
            "%c: %s\n"
            "Vida: %.2lf\n"
            "Ammo: %i\n"
            "\n", p.letra, p.nombre, p.vida, p.municion);
}

void copiar (struct TPersonaje *d, struct TPersonaje o) {
    d->letra = o.letra;
    d->vida = o.vida;
    d->municion = o.municion;
    d->nombre =  (char *) malloc (MAX * sizeof (char));
    strcpy (d->nombre, o.nombre);
}

int main (int argc, char *argv[]) {

   struct TPersonaje mario_bros = { 'M', 0x84, .9, "Mario" },
                     luigi = mario_bros;

   copiar (&luigi, mario_bros);
   luigi.letra = 'L';
   luigi.vida = 1.;
   strcpy (luigi.nombre, "Luigi");

/*
      municion
 ________/\_____
/               \
+---+---+---+---+---+---+---+---+---+---+---+---+
| 84| 00| 00| 00| 00| 00| 00| 00| 00| 00| 00| 00|
+---+---+---+---+---+---+---+---+---+---+---+---+
mario_bros
1000
*/

   struct TPersonaje *puntero = &luigi;

   (*puntero).vida = 7;
   /* Ideas tan plausibles como peregrinas.
   puntero[1] = 7;
   *(puntero + 1) = 7;
*/
   puntero->vida = .8;

   muestra (mario_bros);
   muestra (luigi);

    return EXIT_SUCCESS;
}

