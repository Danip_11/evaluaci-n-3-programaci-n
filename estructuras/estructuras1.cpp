#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 															//Librer�a para usar usleep

struct TCoordenada {															//Crea la estructura para almacenar los diferentes datos
    double x;
    double y;
};

struct TCoordenada preguntar (const char *nombre) {								//Funciona como una funcion a la cual le van a pasar un string
    struct TCoordenada info;													//Hace refencia a TCoordenada para usar su estructura

    printf ("%s.x = ", nombre);													//Imprime el string del nombre pasado
    scanf ("%lf", &info.x);														//Recoge el valor introducio y lo guarda
    printf ("%s.y = ", nombre);													//""...
    scanf ("%lf", &info.y);														//""...
    printf ("\n");

    return info;																//Devuelve los valores almacenados cuando es llamada 
}

void imprimir (const char *nombre_preguntar, struct TCoordenada dato) {			//Funci�n a la cual le pasan un nombre y un dato a imprimir 
    printf ("%s: (%.2lf, %.2lf)\n", nombre_preguntar, dato.x, dato.y);			//Imprime el nombre pasado y los datos aproximados a dos decimales
    printf ("\n");
}

int main () {

    struct TCoordenada posicion, velocidad;		//Hace referencia a la estructura creada arriba para poder usarla

    posicion = preguntar ("Posicion");			//Mete en posicion los valores que devuelva TCoordenada preguntar a la cual le pasamos un texto  
    velocidad = preguntar ("Velocidad");		//""... 

    do{
        posicion.x += velocidad.x;				//Suma los valores de posicion.x m�s los de velocidad.y
        posicion.y += velocidad.y;				//""...
        imprimir ("Posicion", posicion);		//Llama a la funci�n imprimir a la cual le pasa el string con el nombre y el resultado del c�lculo
        imprimir ("Velocidad", velocidad);      //""...
        usleep (100000); 						//Suspende la ejecuci�n hasta que haya transcurrido el tiempo especificado en microsegundos
    }while (1); 								//Bucle infinito

    return EXIT_SUCCESS;
}


